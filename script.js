const passwordInput = document.getElementById('passwordInput');
const confirmPasswordInput = document.getElementById('confirmPasswordInput');
const showPasswordIcon = document.getElementById('showPassword');
const showConfirmPasswordIcon = document.getElementById('showConfirmPassword');

function togglePasswordVisibility(input, icon) {
  if (input.type === 'password') {
    input.type = 'text';
    icon.classList.remove('fa-eye');
    icon.classList.add('fa-eye-slash');
  } else {
    input.type = 'password';
    icon.classList.remove('fa-eye-slash');
    icon.classList.add('fa-eye');
  }
}

showPasswordIcon.addEventListener('click', function () {
  togglePasswordVisibility(passwordInput, showPasswordIcon);
});

showConfirmPasswordIcon.addEventListener('click', function () {
  togglePasswordVisibility(confirmPasswordInput, showConfirmPasswordIcon);
});

const form = document.querySelector('.password-form');
const errorText = document.createElement('span');
errorText.textContent = 'Потрібно ввести однакові значення';
errorText.style.color = 'red';

form.addEventListener('submit', function (event) {
  event.preventDefault();

  if (passwordInput.value === confirmPasswordInput.value) {
    alert('You are welcome');
    if (confirmPasswordInput.parentElement.querySelector('span')) {
      confirmPasswordInput.parentElement.removeChild(errorText);
    }
  } else {
    if (!confirmPasswordInput.parentElement.querySelector('span')) {
      confirmPasswordInput.parentElement.appendChild(errorText);
    }
  }
});
